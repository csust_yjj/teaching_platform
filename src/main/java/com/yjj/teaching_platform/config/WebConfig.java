package com.yjj.teaching_platform.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Author: YJJ
 * Description:
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${image_save_path}")
    private String imageSavePath;
    @Value("${video_save_path}")
    private String videoSavePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**")
                .addResourceLocations("file:"+imageSavePath);
        registry.addResourceHandler("/videos/**")
                .addResourceLocations("file:"+videoSavePath);
    }
}
