package com.yjj.teaching_platform.config;

import com.github.pagehelper.PageHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Author: YJJ
 * Description:
 */
@Configuration
public class PageHelperConfig {

    @Bean
    public PageHelper pageHelper(){

        return new PageHelper();
    }
}
