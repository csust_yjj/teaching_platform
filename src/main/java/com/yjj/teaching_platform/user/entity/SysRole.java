package com.yjj.teaching_platform.user.entity;

import lombok.Data;

/**
 * <p></p>
 *
 * @author yangjunjie
 * @author 其他作者姓名
 * @version 1.00 2020/04/22 yangjunjie 创建
 * <p>1.01 YYYY/MM/DD 修改者姓名 修改内容说明</p>
 */
@Data
public class SysRole {

    private Integer id;

    private String roleName;
}
