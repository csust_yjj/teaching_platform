package com.yjj.teaching_platform.user.entity;

import lombok.Data;

/**
 * @author yangjunjie
 */
@Data
public class SysUser {

    private Integer id;

    private String username;

    private String password;

    private String nickName;

    private String email;

    private String qq;

    private Integer roleId;
}
