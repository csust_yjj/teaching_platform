package com.yjj.teaching_platform.user.dao;

import com.yjj.teaching_platform.user.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * <p></p>
 *
 * @author yangjunjie
 * @author 其他作者姓名
 * @version 1.00 2020/04/22 yangjunjie 创建
 * <p>1.01 YYYY/MM/DD 修改者姓名 修改内容说明</p>
 */
@Mapper
@Component
public interface SysRoleMapper {

    @Select("select * form sys_role where id = #{id}")
    SysRole getRoleById(Integer id);
}
