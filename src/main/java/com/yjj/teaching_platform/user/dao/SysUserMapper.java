package com.yjj.teaching_platform.user.dao;

import com.yjj.teaching_platform.user.entity.SysUser;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yangjunjie
 */
@Mapper
@Component
public interface SysUserMapper {

    @Select("select * from sys_user where username = #{username}")
    SysUser getUserByName(String username);

    @Insert("insert into sys_user(username,password,role_id)values(#{username},#{password},#{roleId})")
    int insertByObject(SysUser sysUser);

    @Update("update sys_user set email = #{email},qq = #{qq} where id = #{id}")
    int updateSysUser(SysUser sysUser);
}
