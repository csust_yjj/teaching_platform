package com.yjj.teaching_platform.user.service;

import com.sun.org.apache.bcel.internal.generic.RETURN;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.user.entity.SysRole;
import com.yjj.teaching_platform.user.entity.SysUser;

import javax.servlet.http.HttpServletRequest;

/**
 * Author:YJJ
 * Description:用户相关
 */

public interface SysUserService {

    SysUser getUserByName(String username);

    SysRole getRoleById(Integer id);

    ReturnObject registry(String username, String password, String repeatPass, String verifyCode, HttpServletRequest request);
}
