package com.yjj.teaching_platform.user.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ErrorEnum;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.user.dao.SysRoleMapper;
import com.yjj.teaching_platform.user.dao.SysUserMapper;
import com.yjj.teaching_platform.user.entity.SysRole;
import com.yjj.teaching_platform.user.entity.SysUser;
import com.yjj.teaching_platform.user.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Author: YJJ
 * Description:
 */
@Service
@Slf4j
public class SysSysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public SysUser getUserByName(String username) {

        return sysUserMapper.getUserByName(username);
    }

    @Override
    public SysRole getRoleById(Integer id) {
        return sysRoleMapper.getRoleById(id);
    }

    @Override
    public ReturnObject registry(String username, String password, String repeatPass, String verifyCode, HttpServletRequest request) {

        if (sysUserMapper.getUserByName(username) != null) {

            throw new BaseException(ErrorEnum.USERNAME_EXISTED);
        } else if (!password.equals(repeatPass)) {

            throw new BaseException(ErrorEnum.PASSWORD_REPEAT_ERROR);
        } else if (!request.getAttribute("verify_session_code").equals(verifyCode)) {

            throw new BaseException(ErrorEnum.VERIFY_CODE_ERROR);
        }
        return ReturnObject.success("注册成功");
    }
}
