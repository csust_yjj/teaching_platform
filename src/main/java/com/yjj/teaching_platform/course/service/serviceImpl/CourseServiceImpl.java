package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.CourseMapper;
import com.yjj.teaching_platform.course.dao.QuestionMapper;
import com.yjj.teaching_platform.course.dao.TaskMapper;
import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.service.CourseService;
import com.yjj.teaching_platform.user.entity.SysUser;
import com.yjj.teaching_platform.utils.FileUploadUtil;
import com.yjj.teaching_platform.utils.RandomUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Author: YJJ
 * Description:课程服务实现类
 */
@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public List<Course> getCourseList(Integer userId) {
        return  courseMapper.getCourseByUserId(userId);

    }

    @Override
    public Course getCourse(Integer courseId) {

        return courseMapper.getCourseById(courseId);
    }

    @Override
    public void addCourse(Integer userId,Course course) {

        // 上传背景图 返回url
        courseMapper.insertByObject(course);
        courseMapper.insertCourseUserRelation(userId,course.getId());
    }

    @Override
    public void update(Course course) {

        courseMapper.updateByObject(course);
    }


    @Transactional
    @Override
    public void deleteCourse(Integer courseId) {

        courseMapper.deleteById(courseId);
        courseMapper.deleteCourseUserRelation(courseId);
        taskMapper.deleteTask(courseId);
        questionMapper.deleteByCourseId(courseId);

    }

}
