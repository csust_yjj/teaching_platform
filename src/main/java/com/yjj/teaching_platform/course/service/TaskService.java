package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.TaskInfo;
import com.yjj.teaching_platform.course.eneity.VO.TaskInfoVO;
import com.yjj.teaching_platform.course.eneity.VO.TaskQuestionVO;

import java.util.List;
import java.util.Map;

/**
 * Author: YJJ
 * Description:作业Service
 */
public interface TaskService {

    /**
     * 获取当前课程作业列表
     * @return
     */
    Map<String,Object> getTaskList(Integer courseId, Integer page);


    /**
     * 添加作业到作业库中
     * @return
     */
    ReturnObject addTask(Integer courseId,String taskName,String deadlineTime,Integer questionCount,List<Integer> difficulty,List<Integer> typeList,List<Integer> sectionList);


    void deleteTask(Integer taskId);

    TaskQuestionVO taskDetail(Integer taskId);

    /**
     * 发布作业
     * @return
     */
    void submitTask(Integer taskId);

    /**
     * 将题目加入作业中
     * @return
     */
    ReturnObject addQuestionToTask(Integer taskId,Integer sectionId, Integer type, Integer difficulty);


}
