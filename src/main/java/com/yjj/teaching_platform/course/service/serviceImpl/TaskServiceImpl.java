package com.yjj.teaching_platform.course.service.serviceImpl;

import com.github.pagehelper.PageHelper;
import com.yjj.teaching_platform.common.base.ConstMap;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.ChapterSectionMapper;
import com.yjj.teaching_platform.course.dao.QuestionMapper;
import com.yjj.teaching_platform.course.dao.TaskMapper;
import com.yjj.teaching_platform.course.eneity.QuestionInfo;
import com.yjj.teaching_platform.course.eneity.TaskInfo;
import com.yjj.teaching_platform.course.eneity.VO.QuestionInfoVO;
import com.yjj.teaching_platform.course.eneity.VO.TaskInfoVO;
import com.yjj.teaching_platform.course.eneity.VO.TaskQuestionVO;
import com.yjj.teaching_platform.course.service.QuestionService;
import com.yjj.teaching_platform.course.service.TaskService;
import org.apache.shiro.crypto.hash.Hash;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private ChapterSectionMapper chapterSectionMapper;

    @Override
    public Map<String,Object> getTaskList(Integer courseId,Integer page) {
        List<TaskInfoVO> taskInfoVOList = new ArrayList<>();
        PageHelper.startPage(page,10);
        List<TaskInfo> taskInfoList =  taskMapper.getTaskInfoList(courseId);
        Integer total = taskMapper.getCountByCourseId(courseId);
        for (TaskInfo taskInfo : taskInfoList) {
            TaskInfoVO taskInfoVO = new TaskInfoVO();
            BeanUtils.copyProperties(taskInfo,taskInfoVO);
            taskInfoVO.setStatusId(taskInfo.getStatus());
            taskInfoVO.setStatusName(ConstMap.TASK_STATUS.get(taskInfo.getStatus()));
            taskInfoVOList.add(taskInfoVO);
        }
        Map<String,Object> result = new HashMap<>();
        result.put("total",total);
        result.put("data",taskInfoVOList);
        return result;
    }

    @Transactional
    @Override
    public ReturnObject addTask(Integer courseId, String taskName, String deadlineTime, Integer questionCount, List<Integer> difficultyList, List<Integer> typeList, List<Integer> sectionList) {


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TaskInfo taskInfo = new TaskInfo();
        try {
            taskInfo.setCourseId(courseId);
            taskInfo.setCreateTime(new Date());
            taskInfo.setTaskName(taskName);
            taskInfo.setStatus(1);
            Date deadTime = simpleDateFormat.parse(deadlineTime);
            taskInfo.setDeadlineTime(deadTime);
            taskMapper.insertByObject(taskInfo);
        }catch (ParseException e){
            e.printStackTrace();
        }

        // 在符合条件的题目中 抽取一定数量的题目
        List<Integer> questionIdList = questionService.getQuestionIdByOptions(difficultyList,typeList,sectionList);
        if(questionIdList.size() < questionCount){
            return ReturnObject.fail(108,"题目数量不足，请先添加题目");
        }
        Random random = new Random();
        Set<Integer> questionResultSet = new HashSet<>();
        while(questionResultSet.size() < questionCount) {
            questionResultSet.add(questionIdList.get(random.nextInt(questionIdList.size())));
        }
        StringBuilder result = new StringBuilder("");
        Iterator iterator = questionResultSet.iterator();
        while(iterator.hasNext()){
            Integer id = (Integer)iterator.next();
            if(!iterator.hasNext()){
                 result.append(id);
                 break;
            }
            result.append(id);
            result.append(",");
        }
        taskMapper.updateQuestionIdList(taskInfo.getId(),result.toString());
        return ReturnObject.success();

    }

    @Transactional
    @Override
    public void deleteTask(Integer taskId) {

        taskMapper.deleteTask(taskId);
    }

    @Override
    public TaskQuestionVO taskDetail(Integer taskId) {

        TaskQuestionVO taskQuestionVO = new TaskQuestionVO();
        TaskInfo taskInfo = taskMapper.getOneById(taskId);
        taskQuestionVO.setDeadlineTime(taskInfo.getDeadlineTime());
        taskQuestionVO.setTaskName(taskInfo.getTaskName());
        String[] questionIds = taskMapper.getQuestionsByTaskId(taskId).split(",");
        List<Integer> questionIdList = new ArrayList<>();
        String title;
        for (int i = 0; i < questionIds.length; i++) {
            questionIdList.add(Integer.parseInt(questionIds[i]));
        }
        List<QuestionInfo> questionInfoList = questionMapper.getQuestionByIds(questionIdList);
        List<QuestionInfoVO> questionInfoVOList = new ArrayList<>();
        for (QuestionInfo questionInfo : questionInfoList) {
            QuestionInfoVO questionInfoVO = new QuestionInfoVO();
            BeanUtils.copyProperties(questionInfo,questionInfoVO);
            questionInfoVO.setTypeName(questionInfo.getType() == 1?"选择题":"判断题");
            questionInfoVO.setDifficultyName(ConstMap.QUESTION_DIFFICULTY_MAP.get(questionInfo.getDifficulty()));
            title = chapterSectionMapper.getSectionTitleById(questionInfo.getSectionId());
            questionInfoVO.setSectionId(questionInfo.getSectionId());
            questionInfoVO.setSectionName(title == null?"暂无":title);
            questionInfoVOList.add(questionInfoVO);
        }
        taskQuestionVO.setQuestionInfoVOList(questionInfoVOList);
        return taskQuestionVO;
    }

    @Override
    public void submitTask(Integer taskId) {

        taskMapper.updateStatusByTaskId(taskId,2);
    }

    @Override
    public ReturnObject addQuestionToTask(Integer taskId,Integer sectionId, Integer type, Integer difficulty) {


        return ReturnObject.success();
    }
}
