package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.CourseChapter;
import com.yjj.teaching_platform.course.eneity.VO.ChapterSectionVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
public interface CourseChapterService {


    /**
     * 获取章节目录
     * @param courseId
     * @return
     */
    List<ChapterSectionVO> getChapterSection(Integer courseId);

    /**
     * 添加章
     * @param title
     * @return
     */
    void addChapter(Integer courseId,String title);

    /**
     * 更新章标题
     * @param id
     * @param title
     */
    void updateChapterTitle(Integer id,String title);

    /**
     * 删除章
     * @param id
     * @return
     */
    void deleteChapter(Integer id);

    /**
     * 添加章教学资源
     * @param file
     * @return
     */
    ReturnObject addFile(MultipartFile file);

    /**
     * 查看章教学资源
     * @param id
     * @return
     */
    ReturnObject getFile(Integer id);


    /**
     * 更新教学资源
     * @param file
     * @return
     */
    ReturnObject updateFile(MultipartFile file);
}
