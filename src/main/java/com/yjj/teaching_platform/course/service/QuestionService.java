package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.QuestionInfo;
import com.yjj.teaching_platform.course.eneity.VO.QuestionInfoVO;

import java.util.List;
import java.util.Map;

/**
 * Author: YJJ
 * Description:题目Service
 */
public interface QuestionService {


    Map<String,Object> getQuestionList(Integer courseId, Integer page);

    /**
     * 根据 难度 题型 知识点的条件筛选题目
     * @param difficultyList
     * @param typeList
     * @param sectionIdList
     * @return
     */
    List<Integer> getQuestionIdByOptions(List<Integer> difficultyList,List<Integer> typeList,List<Integer> sectionIdList);

    /**
     * 添加题目
     * @return
     */
    void addQuestion(QuestionInfo questionInfo);

    /**
     * 更新题目
     * @return
     */
    void updateQuestion(QuestionInfo questionInfo);

    /**
     * 删除题目
     * @return
     */
    void deleteQuestion(Integer id);

}
