package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.ChapterSectionMapper;
import com.yjj.teaching_platform.course.dao.CourseChapterMapper;
import com.yjj.teaching_platform.course.eneity.ChapterSection;
import com.yjj.teaching_platform.course.eneity.CourseChapter;
import com.yjj.teaching_platform.course.service.ChapterSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: YJJ
 * Description: 章-节Service
 */
@Service
public class ChapterSectionServiceImpl implements ChapterSectionService {

    @Autowired
    private ChapterSectionMapper chapterSectionMapper;
    @Autowired
    private CourseChapterMapper courseChapterMapper;

    @Override
    public List<ChapterSection> getSection(Integer chapterId) {

        return chapterSectionMapper.getSectionByChapterId(chapterId);
    }

    @Override
    public List<ChapterSection> getSectionByCourseId(Integer courseId) {

        List<ChapterSection> chapterSectionList = new ArrayList<>();
        List<CourseChapter> courseChapterList = courseChapterMapper.getCourseChapterByCourseId(courseId);
        for (CourseChapter courseChapter : courseChapterList) {
            List<ChapterSection> chapterSections = chapterSectionMapper.getSectionByChapterId(courseChapter.getId());
            chapterSectionList.addAll(chapterSections);
        }
        return chapterSectionList;
    }

    @Override
    public void updateSectionTitle(Integer id, String title) {
        chapterSectionMapper.updateChapterSectionTitle(id,title);
    }

    @Override
    public void addSection(Integer id, String title) {

        chapterSectionMapper.insertSection(id,title);
    }

    @Override
    public void deleteSection(Integer id) {

         chapterSectionMapper.deleteSectionById(id);
    }

    @Override
    public ReturnObject addFile(MultipartFile file) {
        return null;
    }

    @Override
    public ReturnObject getFile(Integer id) {
        return null;
    }

    @Override
    public void updateFile(Integer id, String fileName, String filePath) {

        chapterSectionMapper.updateChapterSectionFile(id,fileName,filePath);
    }
}
