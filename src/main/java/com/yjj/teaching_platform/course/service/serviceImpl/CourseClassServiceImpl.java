package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.CourseClassMapper;
import com.yjj.teaching_platform.course.eneity.CourseClass;
import com.yjj.teaching_platform.course.service.CourseClassService;
import com.yjj.teaching_platform.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class CourseClassServiceImpl implements CourseClassService {

    @Autowired
    private CourseClassMapper courseClassMapper;
    @Override
    public List<CourseClass> getCourseListByCourseId(Integer courseId) {

        return courseClassMapper.getByCourseId(courseId);
    }

    @Override
    public void addCourseClass(CourseClass courseClass) {


        CourseClass courseClass1 = courseClassMapper.getByClassName(courseClass.getClassName());
        if(courseClass1 != null){
            throw new BaseException("已存在同名班级");
        }
        String key = RandomUtil.randomKey();
        courseClass.setClassKey(key);
        courseClassMapper.insertByObject(courseClass);
    }
}
