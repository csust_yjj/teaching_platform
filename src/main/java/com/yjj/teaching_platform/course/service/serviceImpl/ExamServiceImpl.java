package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.dao.ExamMapper;
import com.yjj.teaching_platform.course.dao.QuestionMapper;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import com.yjj.teaching_platform.course.eneity.TaskInfo;
import com.yjj.teaching_platform.course.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    private ExamMapper examMapper;

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public ReturnObject getExamList(Integer courseId, Integer status) {

        List<ExamInfo> examInfoList = examMapper.getExamInfo(courseId);
        return ReturnObject.success(examInfoList);
    }

    @Override
    public ReturnObject submitExam(ExamInfo examInfo) {

        return null;
    }

    @Override
    public ReturnObject addQuestionToExam(Integer taskId, Integer sectionId, Integer type, Integer difficulty) {
        return null;
    }
}
