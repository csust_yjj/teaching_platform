package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.service.TaskResultService;
import org.springframework.stereotype.Service;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class TaskResultImpl implements TaskResultService {


    @Override
    public ReturnObject showTask(Integer taskId) {
        return null;
    }

    @Override
    public ReturnObject submitTaskResult() {
        return null;
    }

    @Override
    public ReturnObject correctingTask() {
        return null;
    }
}
