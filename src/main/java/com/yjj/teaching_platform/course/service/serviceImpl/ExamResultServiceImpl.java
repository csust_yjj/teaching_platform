package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import com.yjj.teaching_platform.course.service.ExamService;
import org.springframework.stereotype.Service;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class ExamResultServiceImpl implements ExamService {

    @Override
    public ReturnObject getExamList(Integer courseId, Integer status) {
        return null;
    }

    @Override
    public ReturnObject submitExam(ExamInfo examInfo) {
        return null;
    }

    @Override
    public ReturnObject addQuestionToExam(Integer taskId, Integer sectionId, Integer type, Integer difficulty) {
        return null;
    }
}
