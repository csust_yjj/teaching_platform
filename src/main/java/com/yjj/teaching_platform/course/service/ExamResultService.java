package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;

/**
 * Author: YJJ
 * Description:
 */
public interface ExamResultService {

    /**
     * 查看考试详情
     * @return
     */
    ReturnObject showExam();

    /**
     * 交卷
     * @return
     */
    ReturnObject submitExamResult();

    /**
     * 批改考试
     * @return
     */
    ReturnObject correctingExam();
}
