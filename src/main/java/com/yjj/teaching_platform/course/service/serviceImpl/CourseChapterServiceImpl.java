package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.ChapterSectionMapper;
import com.yjj.teaching_platform.course.dao.CourseChapterMapper;
import com.yjj.teaching_platform.course.eneity.ChapterSection;
import com.yjj.teaching_platform.course.eneity.CourseChapter;
import com.yjj.teaching_platform.course.eneity.VO.ChapterSectionVO;
import com.yjj.teaching_platform.course.service.CourseChapterService;
import com.yjj.teaching_platform.utils.BeanUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: YJJ
 * Description:课程-章 Service
 */
@Service
public class CourseChapterServiceImpl implements CourseChapterService {

    @Autowired
    private CourseChapterMapper courseChapterMapper;

    @Autowired
    private ChapterSectionMapper chapterSectionMapper;

    @Override
    public List<ChapterSectionVO> getChapterSection(Integer courseId) {
        List<ChapterSectionVO> chapterSectionVOList = new ArrayList<>();
        List<CourseChapter> courseChapterList= courseChapterMapper.getCourseChapterByCourseId(courseId);
        for (CourseChapter courseChapter : courseChapterList) {
            ChapterSectionVO chapterSectionVO = new ChapterSectionVO();
            BeanUtils.copyProperties(courseChapter,chapterSectionVO);
            chapterSectionVO.setChapterSectionList(chapterSectionMapper.getSectionByChapterId(courseChapter.getId()));
            chapterSectionVOList.add(chapterSectionVO);
        }
        return chapterSectionVOList;
    }


    @Override
    public void addChapter(Integer courseId,String title) {

        courseChapterMapper.insertCourseChapter(courseId,title);
    }

    @Override
    public void updateChapterTitle(Integer id, String title) {
        courseChapterMapper.updateCourseChapterTitle(id,title);
    }

    @Override
    public void deleteChapter(Integer id) {

        List<ChapterSection> chapterSections = chapterSectionMapper.getSectionByChapterId(id);
        for (ChapterSection chapterSection : chapterSections) {
            chapterSectionMapper.deleteSectionById(chapterSection.getId());
        }
        courseChapterMapper.deleteCourseChapterById(id);
    }

    @Override
    public ReturnObject addFile(MultipartFile file) {
        return null;
    }

    @Override
    public ReturnObject getFile(Integer id) {
        return null;
    }

    @Override
    public ReturnObject updateFile(MultipartFile file) {
        return null;
    }
}
