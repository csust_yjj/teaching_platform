package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.ChapterSection;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Author: YJJ
 * Description: 小节服务
 */
public interface ChapterSectionService {

    /**
     * 获取小节
     * @param chapterId
     * @return
     */
    List<ChapterSection> getSection(Integer chapterId);

    List<ChapterSection> getSectionByCourseId(Integer courseId);

    /**
     * 更新节标题
     * @param id
     * @param title
     */
    void updateSectionTitle(Integer id,String title);
    /**
     * 添加小节
     * @param title
     * @return
     */
    void addSection(Integer id,String title);

    /**
     * 删除小节
     * @param id
     * @return
     */
    void deleteSection(Integer id);

    /**
     * 添加小节教学资源
     * @param file
     * @return
     */
    ReturnObject addFile(MultipartFile file);

    /**
     * 查看小节教学资源
     * @param id
     * @return
     */
    ReturnObject getFile(Integer id);


    /**
     * 更新小节教学资源
     * @param file
     * @return
     */
    void updateFile(Integer id,String fileName,String filePath);
}
