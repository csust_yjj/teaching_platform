package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;

/**
 * Author: YJJ
 * Description:
 */
public interface TaskResultService {

    /**
     * 查看作业详情
     * @return
     */
    ReturnObject showTask(Integer taskId);

    /**
     * 交作业
     * @return
     */
    ReturnObject submitTaskResult();

    /**
     * 批改作业
     * @return
     */
    ReturnObject correctingTask();
}
