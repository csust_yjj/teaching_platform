package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.Course;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Author: YJJ
 * Description: 课程服务
 */
public interface CourseService {

    /**
     * 根据用户id 查看关联的课程列表
     * @param userId
     * @return
     */
    List<Course> getCourseList(Integer userId);

    Course getCourse(Integer courseId);
    /**
     * 教师添加课程
     * @param course
     * @return
     */
    void addCourse(Integer userId,Course course );


    void update(Course course);


    /**
     * 移除课程
     * @return
     */
    void deleteCourse(Integer courseId);


}
