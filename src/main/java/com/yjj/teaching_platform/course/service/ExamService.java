package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.ExamInfo;

/**
 * Author: YJJ
 * Description:
 */
public interface ExamService {

    /**
     * 获取当前课程考试列表
     * @return
     */
    ReturnObject getExamList(Integer courseId,Integer status);


    /**
     * 发布考试
     * @return
     */
    ReturnObject submitExam(ExamInfo examInfo);

    /**
     * 将题目加入考试中
     * @return
     */
    ReturnObject addQuestionToExam(Integer taskId,Integer sectionId, Integer type, Integer difficulty);

}
