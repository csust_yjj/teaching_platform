package com.yjj.teaching_platform.course.service;

import com.yjj.teaching_platform.course.eneity.CourseClass;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
public interface CourseClassService {

    /**
     * 获取某一课程下的所有班级
     * @param courseId
     * @return
     */
    List<CourseClass> getCourseListByCourseId(Integer courseId);

    /**
     * 新增一个班级
     * @param courseClass
     */
    void addCourseClass(CourseClass courseClass);
}
