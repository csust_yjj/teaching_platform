package com.yjj.teaching_platform.course.service.serviceImpl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.yjj.teaching_platform.common.base.ConstMap;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.common.base.exception.BaseException;
import com.yjj.teaching_platform.course.dao.ChapterSectionMapper;
import com.yjj.teaching_platform.course.dao.QuestionMapper;
import com.yjj.teaching_platform.course.eneity.QuestionInfo;
import com.yjj.teaching_platform.course.eneity.VO.QuestionInfoVO;
import com.yjj.teaching_platform.course.service.QuestionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: YJJ
 * Description:
 */
@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private ChapterSectionMapper chapterSectionMapper;

    @Override
    public Map<String,Object> getQuestionList(Integer courseId, Integer page) {

         PageHelper.startPage(page,10);
         List<QuestionInfo> questionInfoList = questionMapper.getQuestionByCourseId(courseId);
         List<QuestionInfoVO> questionInfoVOList = new ArrayList<>();
         String title;
        for (QuestionInfo questionInfo : questionInfoList) {
            QuestionInfoVO questionInfoVO = new QuestionInfoVO();
            BeanUtils.copyProperties(questionInfo,questionInfoVO);
            questionInfoVO.setTypeId(questionInfo.getType());
            questionInfoVO.setTypeName(questionInfo.getType() == 1?"选择题":"判断题");
            questionInfoVO.setDifficultyId(questionInfo.getDifficulty());
            questionInfoVO.setDifficultyName(ConstMap.QUESTION_DIFFICULTY_MAP.get(questionInfo.getDifficulty()));
            title = chapterSectionMapper.getSectionTitleById(questionInfo.getSectionId());
            questionInfoVO.setSectionId(questionInfo.getSectionId());
            questionInfoVO.setSectionName(title == null?"暂无":title);
            questionInfoVOList.add(questionInfoVO);
        }
        Map<String,Object> result = new HashMap();
        result.put("total",questionMapper.getQuestionCount(courseId));
        result.put("data",questionInfoVOList);
        return result;
    }

    @Override
    public List<Integer> getQuestionIdByOptions(List<Integer> difficultyList, List<Integer> typeList, List<Integer> sectionIdList) {

        List<Integer> questionIdList = questionMapper.selectQuestionByOptions(difficultyList,typeList,sectionIdList);
        return questionIdList;
    }

    @Override
    public void addQuestion(QuestionInfo questionInfo) {

        questionMapper.insertByObject(questionInfo);
    }

    @Override
    public void updateQuestion(QuestionInfo questionInfo) {

        questionMapper.updateByObject(questionInfo);
    }

    @Override
    public void deleteQuestion(Integer id) {

        questionMapper.deleteById(id);
    }

}
