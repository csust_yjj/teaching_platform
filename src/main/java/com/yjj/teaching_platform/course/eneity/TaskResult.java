package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

import java.util.Date;

/**
 * Author: YJJ
 * Description:作业结果实体类
 */
@Data
public class TaskResult {

    private Integer id;

    private Integer taskId;

    private Integer userId;

    private String answerList;

    private Integer status;

    private Integer score;

    private String comment;

    private Date submitTime;
}
