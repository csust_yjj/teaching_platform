package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

/**
 * Author: YJJ
 * Description:试卷结果实体类
 */
@Data
public class ExamResult {

    private Integer id;

    private Integer examId;

    private Integer userId;

    private String answerList;

    private Integer status;

    private Integer score;
}
