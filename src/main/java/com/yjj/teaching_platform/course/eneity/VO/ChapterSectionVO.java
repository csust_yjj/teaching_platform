package com.yjj.teaching_platform.course.eneity.VO;

import com.yjj.teaching_platform.course.eneity.ChapterSection;
import lombok.Data;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Data
public class ChapterSectionVO {

    private Integer id;

    private Integer courseId;

    private String title;

    private List<ChapterSection> chapterSectionList;
}
