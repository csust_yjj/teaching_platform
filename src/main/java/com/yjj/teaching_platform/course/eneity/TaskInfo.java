package com.yjj.teaching_platform.course.eneity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Author: YJJ
 * Description:作业信息实体类
 */
@Data
public class TaskInfo {

    private Integer id;

    private Integer courseId;

    private String taskName;

    private Integer status;

    // 时间格式接受注解
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date deadlineTime;
}
