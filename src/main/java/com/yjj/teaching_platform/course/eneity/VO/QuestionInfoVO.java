package com.yjj.teaching_platform.course.eneity.VO;

import lombok.Data;

/**
 * Author: YJJ
 * Description:
 */
@Data
public class QuestionInfoVO {

    private Integer id;

    private Integer courseId;

    private Integer typeId;

    private String typeName;

    private String questionDescription;

    private String option1;

    private String option2;

    private String option3;

    private String option4;

    private Integer difficultyId;

    private String difficultyName;

    private Integer sectionId;

    private String sectionName;

    private Integer rightAnswer;
}
