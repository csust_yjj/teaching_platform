package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

import java.util.Date;

/**
 * Author: YJJ
 * Description:试卷信息实体类
 */
@Data
public class ExamInfo {

    private Integer id;

    private Integer courseId;

    private String examName;

    private Integer status;

    private Date startTime;

    private Date deadTime;
}
