package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

/**
 * Author: YJJ
 * Description:课程章实体类
 */
@Data
public class CourseChapter {

    private Integer id;

    private Integer courseId;

    private String title;

}
