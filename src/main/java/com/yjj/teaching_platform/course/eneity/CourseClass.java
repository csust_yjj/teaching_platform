package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

/**
 * Author: YJJ
 * Description:
 */
@Data
public class CourseClass {

    private Integer id;

    private Integer courseId;

    private String className;

    private Integer number;

    private String classKey;
}
