package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

/**
 * Author: YJJ
 * Description:一章当中的每一小节
 */
@Data
public class ChapterSection {

    private Integer id;

    private Integer chapterId;

    private String title;

    private String fileName;

    private String filePath;
}
