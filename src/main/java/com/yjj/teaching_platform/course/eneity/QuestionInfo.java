package com.yjj.teaching_platform.course.eneity;

import lombok.Data;

/**
 * Author: YJJ
 * Description:题目实体类
 */
@Data
public class QuestionInfo {

    private Integer id;

    private Integer courseId;

    private Integer type;

    private String questionDescription;

    private String option1;

    private String option2;

    private String option3;

    private String option4;

    private int difficulty;

    private Integer sectionId;

    private Integer rightAnswer;
}
