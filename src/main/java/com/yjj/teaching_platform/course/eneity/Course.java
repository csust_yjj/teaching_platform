package com.yjj.teaching_platform.course.eneity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Author: YJJ
 * Description: 课程实体类
 */
@Data
public class Course {

    private Integer id;

    private String courseName;

    private String background;

    private String description;

}
