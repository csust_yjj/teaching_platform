package com.yjj.teaching_platform.course.eneity.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Data
public class TaskQuestionVO {

    private String taskName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date deadlineTime;

    private List<QuestionInfoVO> questionInfoVOList;


}
