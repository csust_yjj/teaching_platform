package com.yjj.teaching_platform.course.eneity.VO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Author: YJJ
 * Description:
 */
@Data
public class TaskInfoVO {

    private Integer id;

    private Integer courseId;

    private String taskName;

    private Integer statusId;

    private String statusName;

    // 时间格式返回注解
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date deadlineTime;
}
