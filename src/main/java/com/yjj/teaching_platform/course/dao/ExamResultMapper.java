package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Mapper
@Component
public interface ExamResultMapper {

    String TABLE_NAME = "exam_result";
    String SELECT_FIELDS = "id,exam_id,user_id,result,score";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where exam_id = #{examId}")
    List<Course> getExamInfo(Integer examId);

    @Insert("insert into "+TABLE_NAME+" (exam_id,user_id,result,score)values(" +
            "#{examId},#{userId},#{result},#{score})")
    int insertByObject(ExamInfo examInfo);
}
