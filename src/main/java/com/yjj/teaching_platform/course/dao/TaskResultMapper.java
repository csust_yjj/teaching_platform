package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import com.yjj.teaching_platform.course.eneity.TaskResult;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Mapper
@Component
public interface TaskResultMapper {

    String TABLE_NAME = "task_result";
    String SELECT_FIELDS = "id,task_id,user_id,result,score";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where task_id = #{taskId}")
    List<Course> getTaskInfo(Integer examId);

    @Insert("insert into "+TABLE_NAME+" (task_id,user_id,result,score)values(" +
            "#{taskId},#{userId},#{result},#{score})")
    int insertByObject(TaskResult taskResult);
}
