package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.ChapterSection;
import com.yjj.teaching_platform.course.eneity.CourseChapter;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description: 章-节Mapper
 */
@Mapper
@Component
public interface ChapterSectionMapper {

    String TABLE_NAME = "chapter_section";
    String SELECT_FIELDS = "id,chapter_id,title,file_name,file_path";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME +" where chapter_id = #{chapterId}")
    List<ChapterSection> getSectionByChapterId(Integer chapterId);

    @Select("select title from "+TABLE_NAME +" where id = #{id}")
    String getSectionTitleById(Integer id);

    @Update("update "+TABLE_NAME+"  set title = #{title} where id = #{id}")
    int updateChapterSectionTitle(Integer id,String title);
    @Update("update chapter_section set file_name = #{fileName},file_path = #{filePath} where id = #{id}")
    int updateChapterSectionFile(Integer id,String fileName,String filePath);

    @Insert("insert into "+TABLE_NAME+ "(chapter_id,title)values(#{chapterId},#{title})")
    int insertSection(Integer chapterId,String title);

    @Delete("delete from "+TABLE_NAME+" where id = #{sectionId}")
    int deleteSectionById(Integer sectionId);
}
