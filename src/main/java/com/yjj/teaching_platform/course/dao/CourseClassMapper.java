package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.CourseClass;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Component
@Mapper
public interface CourseClassMapper {

    @Select("select * from class where course_id = #{courseId}")
    List<CourseClass> getByCourseId(Integer courseId);

    @Select("select * from class where class_name =#{className}")
    CourseClass getByClassName(String className);

    @Insert("insert into class (course_id,class_name,class_key)values(#{courseId},#{className},#{classKey})")
    int insertByObject(CourseClass courseClass);
}
