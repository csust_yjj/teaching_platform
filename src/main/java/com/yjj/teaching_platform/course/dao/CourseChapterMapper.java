package com.yjj.teaching_platform.course.dao;

import ch.qos.logback.classic.db.names.TableName;
import com.yjj.teaching_platform.course.eneity.CourseChapter;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Author: YJJ
 * Description:课程-章Mapper
 */
@Mapper
@Component
public interface CourseChapterMapper {

    String TABLE_NAME = "course_chapter";
    String SELECT_FIELDS = "id,course_id,title";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+ " where course_id = #{courseId}")
    List<CourseChapter> getCourseChapterByCourseId(Integer courseId);

    @Update("update "+TABLE_NAME+ " set title = #{title} where id = #{id}")
    int updateCourseChapterTitle(Integer id,String title);


    @Insert("insert into "+TABLE_NAME+ " (course_id,title)values(#{courseId},#{title})")
    int insertCourseChapter(Integer courseId,String title);

    @Delete("delete from "+TABLE_NAME+ " where id = #{chapterId}")
    int deleteCourseChapterById(Integer chapterId);
}
