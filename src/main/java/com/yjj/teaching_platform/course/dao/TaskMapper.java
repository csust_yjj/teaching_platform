package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import com.yjj.teaching_platform.course.eneity.TaskInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:作业Mapper
 */
@Mapper
@Component
public interface TaskMapper {

    String TABLE_NAME = "task_info";
    String SELECT_FIELDS = "id,course_id,task_name,status,create_time,deadline_time";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where course_id = #{courseId} order by status")
    List<TaskInfo> getTaskInfoList(Integer courseId);

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where task_id = #{taskId}")
    List<TaskInfo> getTaskInfoByTaskId(Integer taskId);

    @Select({"select count(1) from ",TABLE_NAME," where course_id = #{courseId}"})
    Integer getCountByCourseId(Integer courseId);

    @Select({"select * from ",TABLE_NAME," where id = #{taskId}"})
    TaskInfo getOneById(Integer taskId);

    @Select({"select question_id_list from ",TABLE_NAME," where id = #{taskId}"})
    String  getQuestionsByTaskId(Integer taskId);

    @Update("update "+TABLE_NAME+" set status = #{status} where id = #{taskId}")
    int updateStatusByTaskId(Integer taskId,Integer status);

    @Insert("insert into "+TABLE_NAME+" (course_id,task_name,status,create_time,deadline_time)values(" +
            "#{courseId},#{taskName},#{status},#{createTime},#{deadlineTime})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    int insertByObject(TaskInfo taskInfo);

    @Update({"update ",TABLE_NAME," set question_id_list = #{questionIdList} where id = #{taskId}"})
    int updateQuestionIdList(Integer taskId,String questionIdList);

    @Delete({"delete from ",TABLE_NAME," where id = #{taskId}"})
    int deleteTask(Integer taskId);

    @Delete({"delete from ",TABLE_NAME," where course_id = #{courseId}"})
    int deleteTaskByCourseId(Integer courseId);

}
