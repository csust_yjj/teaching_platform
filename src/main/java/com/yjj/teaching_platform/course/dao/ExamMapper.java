package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.eneity.ExamInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:考试Mapper
 */
@Mapper
@Component
public interface ExamMapper {

    String TABLE_NAME = "exam_info";
    String SELECT_FIELDS = "id,course_id,exam_name,status,start_time,dead_time";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where course_id = #{courseId}")
    List<ExamInfo> getExamInfo(Integer courseId);

    @Insert("insert into "+TABLE_NAME+" (course_id,exam_name,start_time,dead_time)values(" +
            "#{courseId},#{examName},#{startName},#{deadTime})")
    int insertByObject(ExamInfo examInfo);


}
