package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

/**
 * Author: YJJ
 * Description:课程Mapper
 */
@Mapper
@Component
public interface CourseMapper {

    String TABLE_NAME = "course";
    String SELECT_FIELDS = "id,user_id,course_name,background,description";

    @Select("select * from course a,user_course_relation b where" +
            " a.id = b.course_id and b.user_id = #{userId} ")
    List<Course> getCourseByUserId(Integer userId);

    @Select("select * from "+TABLE_NAME+" where id = #{courseId}")
    Course getCourseById(Integer courseId);



    @Select("select count(*) from user_course_relation where user_id = #{userId} and course_id = #{courseId}")
    int getCountOfUserCourseRelation(Integer userId,Integer courseId);

    @Insert("insert into "+TABLE_NAME+" (course_name,background)values(" +
            "#{courseName},#{background})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    int insertByObject(Course course);

    @Update("update "+TABLE_NAME+" set course_name = #{courseName}, background=#{background}, description = #{description} where id = #{id}")
    int updateByObject(Course course);

    @Insert("insert into user_course_relation (user_id,course_id)values(#{userId},#{courseId})")
    int insertCourseUserRelation(Integer userId,Integer courseId);

    @Delete("delete from "+TABLE_NAME+" where id = #{id}")
    int deleteById(Integer id);

    @Delete("delete from course_user_relation where course_id = #{courseId}")
    int deleteCourseUserRelation(Integer courseId);


}
