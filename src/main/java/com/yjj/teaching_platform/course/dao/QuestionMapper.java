package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.eneity.QuestionInfo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@Component
@Mapper
public interface QuestionMapper {

    String TABLE_NAME = "question_info";
    String SELECT_FIELDS = "id,course_id,type,question_description,option_1,option_2,option_3,option_4,difficulty,section_id,right_answer";

    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where course_id = #{courseId}")
    List<QuestionInfo> getQuestionByCourseId(Integer courseId);
    @Select("select count(1) from "+TABLE_NAME+" where course_id=#{courseId}")
    Integer getQuestionCount(Integer courseId);
    @Select("select "+SELECT_FIELDS+" from "+TABLE_NAME+" where section_id = #{sectionId} and type = #{type}" +
            "and difficulty = #{difficulty}")
    List<QuestionInfo> getQuestionsByOptions(Integer sectionId, Integer type, Integer difficulty);

    @Select({"<script>" +
            "select id from ",TABLE_NAME," where difficulty in" +
            "<foreach item = 'item' index = 'index' collection = 'diffList' open='(' separator=',' close = ')'>" +
            "#{item}" +
            "</foreach> and type in " +
            "<foreach item = 'item' index = 'index' collection = 'typeList' open='(' separator=',' close = ')'>" +
            "#{item}" +
            "</foreach> and section_id in " +
            "<foreach item = 'item' index = 'index' collection = 'sectionList' open='(' separator=',' close = ')'>" +
            "#{item}" +
            "</foreach>" +
            "</script>"})
    List<Integer> selectQuestionByOptions(List<Integer> diffList,List<Integer> typeList,List<Integer> sectionList);

    @Select({"<script>" +
            "select * from ",TABLE_NAME," where id in " +
            "<foreach item = 'item' index = 'index' collection = 'questionIdList' open = '(' separator=',' close=')'> " +
            "#{item}" +
            "</foreach>" +
            "</script>"})
    List<QuestionInfo> getQuestionByIds(@Param("questionIdList") List<Integer> questionIdList);
    @Insert("insert into "+TABLE_NAME+" (course_id,type,question_description,option_1,option_2,option_3,option_4,difficulty,section_id,right_answer)" +
            "values(#{courseId},#{type},#{questionDescription},#{option1},#{option2},#{option3},#{option4},#{difficulty},#{sectionId},#{rightAnswer})")
    int insertByObject(QuestionInfo questionInfo);

    @Update("update "+TABLE_NAME+" set type=#{type},question_description = #{questionDescription},option_1 = #{option1}," +
            "option_2 = #{option2},option_3 = #{option3},option_4 = #{option4},difficulty = #{difficulty},section_id =#{sectionId},right_answer = #{rightAnswer} where id= #{id}")
    int updateByObject(QuestionInfo questionInfo);

    @Delete("delete from "+TABLE_NAME+" where id = #{questionId}")
    int deleteById(Integer questionId);

    @Delete({"delete from ",TABLE_NAME," where course_id = #{courseId}"})
    int deleteByCourseId(Integer courseId);
}
