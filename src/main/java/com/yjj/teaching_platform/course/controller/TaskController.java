package com.yjj.teaching_platform.course.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author: YJJ
 * Description:
 */
@RestController
@RequestMapping("/course/task")
public class TaskController {

    @Autowired
    TaskService taskService;

    @GetMapping("/list/{courseId}/{page}")
    public ReturnObject getTaskList(@PathVariable("courseId")Integer courseId,@PathVariable("page")Integer page) {

        return ReturnObject.success(taskService.getTaskList(courseId,page));
    }

    @PostMapping("/add")
    public ReturnObject addTask(@RequestBody JSONObject jsonObject) {

        Integer courseId = jsonObject.getInteger("courseId");
        String taskName = jsonObject.getString("taskName");
        String deadlineTime = jsonObject.getString("deadlineTime");
        Integer questionCount = jsonObject.getInteger("questionCount");
        List<Integer> questionTypeList = JSONObject.parseArray(jsonObject.getJSONArray("questionType").toJSONString(),Integer.class);
        List<Integer> difficultyList = JSONObject.parseArray(jsonObject.getJSONArray("questionDifficulty").toJSONString(),Integer.class);
        List<Integer> sectionIdList = JSONObject.parseArray(jsonObject.getJSONArray("sectionId").toJSONString(),Integer.class);
        return taskService.addTask(courseId,taskName,deadlineTime,questionCount,difficultyList,questionTypeList,sectionIdList);
    }

    @GetMapping("/delete/{taskId}")
    public ReturnObject deleteTask(@PathVariable("taskId")Integer taskId) {

         taskService.deleteTask(taskId);
         return ReturnObject.success();
    }

    @GetMapping("/submit/{taskId}")
    public ReturnObject submitTask(@PathVariable("taskId")Integer taskId) {

        taskService.submitTask(taskId);
        return ReturnObject.success();
    }

    @GetMapping("/detail/{taskId}")
    public ReturnObject getTaskQuestionList(@PathVariable("taskId")Integer taskId) {

         return ReturnObject.success(taskService.taskDetail(taskId));
    }
}
