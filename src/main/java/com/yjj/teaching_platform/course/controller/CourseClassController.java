package com.yjj.teaching_platform.course.controller;

import com.alibaba.fastjson.JSONObject;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.CourseClass;
import com.yjj.teaching_platform.course.service.CourseClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Author: YJJ
 * Description:
 */
@RestController
@RequestMapping("/course/class")
public class CourseClassController {

    @Autowired
    private CourseClassService courseClassService;

    @GetMapping("/list/{courseId}")
    public ReturnObject getClassList(@PathVariable("courseId")Integer courseId) {

        return ReturnObject.success(courseClassService.getCourseListByCourseId(courseId));
    }


    @PostMapping("/add")
    public ReturnObject addClass(@RequestBody JSONObject jsonObject) {

        CourseClass courseClass = new CourseClass();
        courseClass.setClassName(jsonObject.getString("className"));
        courseClass.setCourseId(jsonObject.getInteger("courseId"));
        courseClassService.addCourseClass(courseClass);
        return ReturnObject.success();
    }
}
