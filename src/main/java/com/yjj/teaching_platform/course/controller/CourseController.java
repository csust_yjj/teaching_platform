package com.yjj.teaching_platform.course.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.dao.ChapterSectionMapper;
import com.yjj.teaching_platform.course.dao.CourseMapper;
import com.yjj.teaching_platform.course.eneity.ChapterSection;
import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.service.CourseService;
import com.yjj.teaching_platform.user.entity.SysUser;
import com.yjj.teaching_platform.utils.FileUploadUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Author: YJJ
 * Description:
 */
@RestController
@RequestMapping("/course")
@Slf4j
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/list/{userId}")
    public ReturnObject getCourseList(@PathVariable("userId") Integer userId) {

        return ReturnObject.success(courseService.getCourseList(userId));
    }

    @GetMapping("/get/{courseId}")
    public ReturnObject getCourse(@PathVariable("courseId") Integer courseId) {

        return ReturnObject.success(courseService.getCourse(courseId));
    }

    @PostMapping("/add")
    public ReturnObject addCourse(@RequestBody JSONObject jsonObject) {

        Course course = new Course();
        Integer userId = Integer.valueOf(jsonObject.getString("userId"));
        course.setCourseName(jsonObject.getString("courseName"));
        course.setBackground(jsonObject.getString("background"));
        courseService.addCourse(userId, course);
        return ReturnObject.success();
    }

    @PostMapping("/uploadBackground")
    public ReturnObject uploadBackground(@RequestParam("file") MultipartFile file, HttpServletRequest request) {

        String url = FileUploadUtil.imageUpload(file, request);
        return ReturnObject.success(url);
    }

    @PostMapping("/update")
    public ReturnObject updateCourse(@RequestBody JSONObject jsonObject) {
        Course course = new Course();
        course.setId(jsonObject.getInteger("id"));
        course.setBackground(jsonObject.getString("background"));
        course.setDescription(jsonObject.getString("description"));
        course.setCourseName(jsonObject.getString("courseName"));
        courseService.update(course);
        return ReturnObject.success();
    }

    @PostMapping("/upload")
    public ReturnObject uploadFile(MultipartFile file, HttpServletRequest request) {

        return ReturnObject.success(FileUploadUtil.imageUpload(file, request));
    }

    @GetMapping("/delete/{courseId}")
    public ReturnObject deleteCourse(@PathVariable("courseId")Integer courseId){

        courseService.deleteCourse(courseId);
        return ReturnObject.success();
    }
}
