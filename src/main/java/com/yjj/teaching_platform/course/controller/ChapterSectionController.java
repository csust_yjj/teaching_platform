package com.yjj.teaching_platform.course.controller;

import com.alibaba.fastjson.JSONObject;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.service.ChapterSectionService;
import com.yjj.teaching_platform.course.service.CourseChapterService;
import com.yjj.teaching_platform.utils.FileUploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Author: YJJ
 * Description:课程目录controller
 */
@RestController
@RequestMapping("/course/menu")
public class ChapterSectionController {

    @Autowired
    CourseChapterService courseChapterService;
    @Autowired
    ChapterSectionService chapterSectionService;

    @GetMapping("/get/{courseId}")
    public ReturnObject getChapterSection(@PathVariable("courseId") Integer courseId) {

        return ReturnObject.success(courseChapterService.getChapterSection(courseId));
    }

    @PostMapping("/uploadFile")
    public ReturnObject uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {

        String url = FileUploadUtil.videoUpload(file, request);
        return ReturnObject.success(url);
    }

    @PostMapping("/chapter/add")
    public ReturnObject addChapter(@RequestBody JSONObject jsonObject) {

        courseChapterService.addChapter(jsonObject.getInteger("courseId"), jsonObject.getString("title"));
        return ReturnObject.success();
    }

    @PostMapping("/section/add")
    public ReturnObject addSection(@RequestBody JSONObject jsonObject) {

        chapterSectionService.addSection(jsonObject.getInteger("id"), jsonObject.getString("title"));
        return ReturnObject.success();
    }

    @GetMapping("/section/list/{courseId}")
    public ReturnObject getSection(@PathVariable("courseId")Integer courseId) {

        return ReturnObject.success(chapterSectionService.getSectionByCourseId(courseId));
    }

    @PostMapping("/chapter/update/title")
    public ReturnObject updateChapterTitle(@RequestBody JSONObject jsonObject) {

        courseChapterService.updateChapterTitle(jsonObject.getInteger("id"), jsonObject.getString("title"));
        return ReturnObject.success();
    }

    @PostMapping("/section/update/title")
    public ReturnObject updateSectionTitle(@RequestBody JSONObject jsonObject) {

        chapterSectionService.updateSectionTitle(jsonObject.getInteger("id"), jsonObject.getString("title"));
        return ReturnObject.success();
    }

    @GetMapping("/chapter/delete/{id}")
    public ReturnObject deleteChapter(@PathVariable("id") Integer id) {

        courseChapterService.deleteChapter(id);
        return ReturnObject.success();
    }

    @GetMapping("/section/delete/{id}")
    public ReturnObject deleteSection(@PathVariable("id") Integer id) {
        chapterSectionService.deleteSection(id);
        return ReturnObject.success();
    }

    @PostMapping("/update/file")
    public ReturnObject updateFile(@RequestBody JSONObject jsonObject) {

        chapterSectionService.updateFile(jsonObject.getInteger("id"), jsonObject.getString("fileName"),
                jsonObject.getString("filePath"));
        return ReturnObject.success();
    }


}
