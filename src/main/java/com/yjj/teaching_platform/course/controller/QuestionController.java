package com.yjj.teaching_platform.course.controller;

import com.alibaba.fastjson.JSONObject;
import com.yjj.teaching_platform.common.base.ReturnObject;
import com.yjj.teaching_platform.course.eneity.QuestionInfo;
import com.yjj.teaching_platform.course.eneity.VO.QuestionInfoVO;
import com.yjj.teaching_platform.course.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: YJJ
 * Description:
 */
@RestController
@RequestMapping("/course/question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping("/list/{courseId}/{page}")
    public ReturnObject getQuestionList(@PathVariable("courseId") Integer courseId, @PathVariable("page") Integer page) {

        return ReturnObject.success(questionService.getQuestionList(courseId, page));
    }

    @PostMapping("/add")
    public ReturnObject addQuestion(@RequestBody JSONObject jsonObject) {

        QuestionInfo questionInfo = new QuestionInfo();
        questionInfo.setCourseId(jsonObject.getInteger("courseId"));
        questionInfo.setQuestionDescription(jsonObject.getString("questionDescription"));
        questionInfo.setType(jsonObject.getInteger("type"));
        questionInfo.setOption1(jsonObject.getString("option1"));
        questionInfo.setOption2(jsonObject.getString("option2"));
        questionInfo.setOption3(jsonObject.getString("option3"));
        questionInfo.setOption4(jsonObject.getString("option4"));
        questionInfo.setDifficulty(jsonObject.getInteger("difficulty"));
        questionInfo.setRightAnswer(jsonObject.getInteger("rightAnswer"));
        questionInfo.setSectionId(jsonObject.getInteger("sectionId"));
        questionService.addQuestion(questionInfo);
        return ReturnObject.success();
    }

    @PostMapping("/update")
    public ReturnObject updateQuestion(@RequestBody JSONObject jsonObject) {

        QuestionInfo questionInfo = new QuestionInfo();
        questionInfo.setId(jsonObject.getInteger("id"));
        questionInfo.setQuestionDescription(jsonObject.getString("questionDescription"));
        questionInfo.setType(jsonObject.getInteger("type"));
        questionInfo.setOption1(jsonObject.getString("option1"));
        questionInfo.setOption2(jsonObject.getString("option2"));
        questionInfo.setOption3(jsonObject.getString("option3"));
        questionInfo.setOption4(jsonObject.getString("option4"));
        questionInfo.setDifficulty(jsonObject.getInteger("difficulty"));
        questionInfo.setRightAnswer(jsonObject.getInteger("rightAnswer"));
        questionInfo.setSectionId(jsonObject.getInteger("sectionId"));
        questionService.updateQuestion(questionInfo);
        return ReturnObject.success();
    }

    @GetMapping("/delete/{id}")
    public ReturnObject deleteQuestion(@PathVariable("id") Integer id) {

        questionService.deleteQuestion(id);
        return ReturnObject.success();
    }
}
