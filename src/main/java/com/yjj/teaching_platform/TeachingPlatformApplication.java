package com.yjj.teaching_platform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeachingPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeachingPlatformApplication.class, args);
    }

}
