package com.yjj.teaching_platform.utils;

import org.springframework.boot.web.reactive.context.ConfigurableReactiveWebApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Author: YJJ
 * Description: 全局使用 获取bean的工具类
 */
public class BeanUtil {


    public static ConfigurableReactiveWebApplicationContext context;



    public static <T> T getBean(Class<T> c) {
        return context.getBean(c);
    }
}
