package com.yjj.teaching_platform.utils;

import java.sql.Timestamp;

/**
 * Author: YJJ
 * Description:
 */
public class TeachFileEntity {

    private String fileName;

    private String size;

    private String type;

    private String path;

    private Timestamp uploadTime;
}
