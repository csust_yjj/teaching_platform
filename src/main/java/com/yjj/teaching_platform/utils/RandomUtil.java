package com.yjj.teaching_platform.utils;

import java.util.Random;

/**
 * Author: YJJ
 * Description:随机工具类
 */
public class RandomUtil {

    public static String randomKey() {
            int length = 10;
            String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random=new Random();
            StringBuffer sb=new StringBuffer();
            for(int i=0;i<length;i++){
                int number=random.nextInt(62);
                sb.append(str.charAt(number));
            }
            return sb.toString();
        }
    }
