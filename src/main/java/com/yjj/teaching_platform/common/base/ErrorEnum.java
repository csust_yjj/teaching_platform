package com.yjj.teaching_platform.common.base;

import lombok.Getter;

/**
 * @author yangjunjie
 */
@Getter
public enum ErrorEnum {

    USERNAME_PASSWORD_ERROR(101,"用户名密码错误"),
    PASSWORD_REPEAT_ERROR(102,"两次密码不一致"),
    USERNAME_EXISTED(103,"用户名已存在"),
    VERIFY_CODE_ERROR(104,"验证码错误"),
    PERMISSION_ERROR(105,"没有权限");
    private final Integer code;

    private final String msg;

    ErrorEnum(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }
}
