package com.yjj.teaching_platform.common.base.exception;

import com.yjj.teaching_platform.common.base.ReturnObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

/**
 * Author: YJJ
 * Description: 同一异常处理类
 */
@ControllerAdvice
@Slf4j
public class ExceptionResolver {

    @ResponseBody
    @ExceptionHandler(value = BaseException.class)
    public ReturnObject BaseExceptionResolver(BaseException e) {

        log.error(e.getMsg());
        return ReturnObject.fail(e.getCode(),e.getMessage());
    }

    @ExceptionHandler(value = SQLException.class)
    public void SqlExceptionResolver(SQLException e) {

        e.printStackTrace();
    }

    @ResponseBody
    @ExceptionHandler(value = Exception.class)
    public ReturnObject ExceptionResolver(Exception e) {
        e.printStackTrace();
        return ReturnObject.fail(400,e.getMessage());
    }
}
