package com.yjj.teaching_platform.common.base;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: YJJ
 * Description: 一些常量值
 */
public class ConstMap {

    /**
     * 题目难度
     */
    public final static Map<Integer,String> QUESTION_DIFFICULTY_MAP = new HashMap<Integer,String>() {
        {
            put(1,"非常简单");
            put(2,"简单");
            put(3,"中等");
            put(4,"困难");
            put(5,"非常困难");
        }
    };

    /**
     * 作业状态
     */

    public final static Map<Integer,String> TASK_STATUS = new HashMap<Integer,String>() {
        {
            put(1,"未发布");
            put(2,"已发布");
        }
    };
}
