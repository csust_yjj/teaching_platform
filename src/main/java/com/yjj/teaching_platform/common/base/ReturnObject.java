package com.yjj.teaching_platform.common.base;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: YJJ
 * @Description: 封装返回值
 */
@Setter
@Getter
public class ReturnObject {

    private Integer code;

    private String message;

    private Object data;

    public ReturnObject(Integer code, String message, Object data){

        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ReturnObject(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    /**
     * 请求成功,返回值无Data参数
     */
    public static ReturnObject success(){
        return new ReturnObject(200,"success");
    }


    /**
     * 请求成功
     * @param data 返回Data参数
     */
    public static ReturnObject success(Object data){

        return new ReturnObject(200,"success", data);
    }

    public static ReturnObject success(String message,Object data){

        return new ReturnObject(200,message, data);
    }

    /**
     * 请求失败
     * @param code 错误码
     * @param message 错误信息
     */
    public static ReturnObject fail(Integer code, String message){
        return new ReturnObject(code,message);
    }

    public static ReturnObject fail(ErrorEnum errorEnum) {
        return new ReturnObject(errorEnum.getCode(),errorEnum.getMsg());
    }

    public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<>();
        result.put("code", this.code);
        result.put("message", this.message);
        result.put("data", this.data);
        return result;
    }

}
