package com.yjj.teaching_platform.common.base.exception;

import com.yjj.teaching_platform.common.base.ErrorEnum;
import lombok.Getter;

/**
 * Author: YJJ
 * Description: 通用异常
 */
@Getter
public class BaseException extends RuntimeException{

    private Integer code;

    private String msg;

    public BaseException(Integer code,String msg) {

        this.code = code;
        this.msg = msg;
    }

    public BaseException(ErrorEnum errorEnum) {

        this.code = errorEnum.getCode();
        this.msg = errorEnum.getMsg();
    }

    public BaseException(String msg) {

        this.code = 505;
        this.msg = msg;
    }

}
