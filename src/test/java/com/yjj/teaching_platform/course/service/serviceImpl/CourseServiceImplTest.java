package com.yjj.teaching_platform.course.service.serviceImpl;

import com.yjj.teaching_platform.course.eneity.Course;
import com.yjj.teaching_platform.course.service.CourseService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Author: YJJ
 * Description:
 */
@SpringBootTest
@RunWith(SpringRunner.class)
class CourseServiceImplTest {

    @Autowired
    private CourseService courseService;

    @Test
    void getCourseList() {

    }

    @Test
    void addCourse() {
    }

    @Test
    void enterCourse() {
    }

    @Test
    void deleteCourse() {
    }

    @Test
    void getKey() {
    }
}