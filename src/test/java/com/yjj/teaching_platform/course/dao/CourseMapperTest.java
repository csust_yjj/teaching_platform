package com.yjj.teaching_platform.course.dao;

import com.yjj.teaching_platform.course.eneity.Course;
import org.apache.shiro.util.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <p></p>
 *
 * @author yangjunjie
 * @author 其他作者姓名
 * @version 1.00 2020/04/22 yangjunjie 创建
 * <p>1.01 YYYY/MM/DD 修改者姓名 修改内容说明</p>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
class CourseMapperTest {

    @Autowired
    private CourseMapper courseMapper;
    @Test
    void getCourseByUserId() {
        List<Course> courseList = courseMapper.getCourseByUserId(1);
        Assert.notNull(courseList);
    }

    @Test
    void getKeyByCourseId() {
    }

    @Test
    void insertByObject() {

    }

    @Test
    void insertCourseUserRelation() {
    }

    @Test
    void deleteById() {
    }
}